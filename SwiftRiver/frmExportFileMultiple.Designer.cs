﻿namespace SwiftRiver
{
    partial class frmExportFileMultiple
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lstFiles = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.chkSkip = new System.Windows.Forms.CheckBox();
            this.chkCompress = new System.Windows.Forms.CheckBox();
            this.chkSWFCompression = new System.Windows.Forms.CheckBox();
            this.lblSWFCompression = new System.Windows.Forms.Label();
            this.trackCompression = new System.Windows.Forms.TrackBar();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackCompression)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(171, 315);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(109, 28);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(9, 24);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(268, 20);
            this.txtName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Name";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRemove);
            this.groupBox1.Controls.Add(this.btnAdd);
            this.groupBox1.Controls.Add(this.lstFiles);
            this.groupBox1.Location = new System.Drawing.Point(9, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(266, 166);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Files";
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(8, 132);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(118, 30);
            this.btnRemove.TabIndex = 3;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(130, 132);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(129, 30);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lstFiles
            // 
            this.lstFiles.FormattingEnabled = true;
            this.lstFiles.Location = new System.Drawing.Point(6, 19);
            this.lstFiles.Name = "lstFiles";
            this.lstFiles.Size = new System.Drawing.Size(254, 108);
            this.lstFiles.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Type";
            // 
            // cboType
            // 
            this.cboType.FormattingEnabled = true;
            this.cboType.Items.AddRange(new object[] {
            "XML",
            "Image",
            "Animation",
            "Audio"});
            this.cboType.Location = new System.Drawing.Point(9, 68);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(266, 21);
            this.cboType.TabIndex = 1;
            this.cboType.Text = "Animation";
            // 
            // chkSkip
            // 
            this.chkSkip.AutoSize = true;
            this.chkSkip.Location = new System.Drawing.Point(171, 272);
            this.chkSkip.Name = "chkSkip";
            this.chkSkip.Size = new System.Drawing.Size(111, 17);
            this.chkSkip.TabIndex = 12;
            this.chkSkip.Text = "Skip file compress";
            this.chkSkip.UseVisualStyleBackColor = true;
            // 
            // chkCompress
            // 
            this.chkCompress.AutoSize = true;
            this.chkCompress.Location = new System.Drawing.Point(171, 292);
            this.chkCompress.Name = "chkCompress";
            this.chkCompress.Size = new System.Drawing.Size(109, 17);
            this.chkCompress.TabIndex = 15;
            this.chkCompress.Text = "JPG Compression";
            this.chkCompress.UseVisualStyleBackColor = true;
            this.chkCompress.CheckedChanged += new System.EventHandler(this.chkCompress_CheckedChanged);
            // 
            // chkSWFCompression
            // 
            this.chkSWFCompression.AutoSize = true;
            this.chkSWFCompression.Location = new System.Drawing.Point(12, 272);
            this.chkSWFCompression.Name = "chkSWFCompression";
            this.chkSWFCompression.Size = new System.Drawing.Size(156, 17);
            this.chkSWFCompression.TabIndex = 16;
            this.chkSWFCompression.Text = "Override SWF Compression";
            this.chkSWFCompression.UseVisualStyleBackColor = true;
            this.chkSWFCompression.CheckedChanged += new System.EventHandler(this.chkSWFCompression_CheckedChanged);
            // 
            // lblSWFCompression
            // 
            this.lblSWFCompression.AutoSize = true;
            this.lblSWFCompression.Location = new System.Drawing.Point(14, 296);
            this.lblSWFCompression.Name = "lblSWFCompression";
            this.lblSWFCompression.Size = new System.Drawing.Size(100, 13);
            this.lblSWFCompression.TabIndex = 18;
            this.lblSWFCompression.Text = "SWF Compression: ";
            // 
            // trackCompression
            // 
            this.trackCompression.Location = new System.Drawing.Point(9, 315);
            this.trackCompression.Maximum = 100;
            this.trackCompression.Name = "trackCompression";
            this.trackCompression.Size = new System.Drawing.Size(156, 45);
            this.trackCompression.TabIndex = 19;
            this.trackCompression.ValueChanged += new System.EventHandler(this.trackCompression_ValueChanged);
            // 
            // frmExportFileMultiple
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 353);
            this.Controls.Add(this.trackCompression);
            this.Controls.Add(this.lblSWFCompression);
            this.Controls.Add(this.chkSWFCompression);
            this.Controls.Add(this.chkCompress);
            this.Controls.Add(this.chkSkip);
            this.Controls.Add(this.cboType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmExportFileMultiple";
            this.Text = "Embed Files";
            this.Load += new System.EventHandler(this.frmExportFileMultiple_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackCompression)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox lstFiles;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboType;
        private System.Windows.Forms.CheckBox chkSkip;
        private System.Windows.Forms.CheckBox chkCompress;
        private System.Windows.Forms.CheckBox chkSWFCompression;
        private System.Windows.Forms.Label lblSWFCompression;
        private System.Windows.Forms.TrackBar trackCompression;
    }
}