﻿namespace SwiftRiver
{
    partial class frmAddExporter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lstFiles = new System.Windows.Forms.ListBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAddSingle = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAddMulti = new System.Windows.Forms.Button();
            this.trackCompression = new System.Windows.Forms.TrackBar();
            this.lblSWFCompression = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackCompression)).BeginInit();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(5, 6);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(6, 23);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(272, 20);
            this.txtName.TabIndex = 0;
            // 
            // txtPath
            // 
            this.txtPath.Enabled = false;
            this.txtPath.Location = new System.Drawing.Point(8, 64);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(235, 20);
            this.txtPath.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Export Path";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Enabled = false;
            this.btnBrowse.Location = new System.Drawing.Point(250, 64);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(25, 19);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Files";
            // 
            // lstFiles
            // 
            this.lstFiles.FormattingEnabled = true;
            this.lstFiles.Location = new System.Drawing.Point(8, 107);
            this.lstFiles.Name = "lstFiles";
            this.lstFiles.Size = new System.Drawing.Size(268, 173);
            this.lstFiles.TabIndex = 6;
            this.lstFiles.SelectedIndexChanged += new System.EventHandler(this.lstFiles_SelectedIndexChanged);
            this.lstFiles.DoubleClick += new System.EventHandler(this.lstFiles_DoubleClick);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(8, 286);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(69, 31);
            this.btnRemove.TabIndex = 5;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAddSingle
            // 
            this.btnAddSingle.Location = new System.Drawing.Point(184, 286);
            this.btnAddSingle.Name = "btnAddSingle";
            this.btnAddSingle.Size = new System.Drawing.Size(92, 31);
            this.btnAddSingle.TabIndex = 3;
            this.btnAddSingle.Text = "Add Single";
            this.btnAddSingle.UseVisualStyleBackColor = true;
            this.btnAddSingle.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(8, 353);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(266, 32);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnAddMulti
            // 
            this.btnAddMulti.Location = new System.Drawing.Point(90, 286);
            this.btnAddMulti.Name = "btnAddMulti";
            this.btnAddMulti.Size = new System.Drawing.Size(92, 31);
            this.btnAddMulti.TabIndex = 4;
            this.btnAddMulti.Text = "Add Multi";
            this.btnAddMulti.UseVisualStyleBackColor = true;
            this.btnAddMulti.Click += new System.EventHandler(this.btnAddMulti_Click);
            // 
            // trackCompression
            // 
            this.trackCompression.Location = new System.Drawing.Point(8, 323);
            this.trackCompression.Maximum = 100;
            this.trackCompression.Name = "trackCompression";
            this.trackCompression.Size = new System.Drawing.Size(104, 45);
            this.trackCompression.TabIndex = 7;
            this.trackCompression.ValueChanged += new System.EventHandler(this.trackCompression_ValueChanged);
            // 
            // lblSWFCompression
            // 
            this.lblSWFCompression.AutoSize = true;
            this.lblSWFCompression.Location = new System.Drawing.Point(118, 323);
            this.lblSWFCompression.Name = "lblSWFCompression";
            this.lblSWFCompression.Size = new System.Drawing.Size(126, 13);
            this.lblSWFCompression.TabIndex = 8;
            this.lblSWFCompression.Text = "SWF Compression: 100%";
            // 
            // frmAddExporter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 391);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblSWFCompression);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.trackCompression);
            this.Controls.Add(this.btnAddMulti);
            this.Controls.Add(this.btnAddSingle);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.lstFiles);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmAddExporter";
            this.Text = "Exporter";
            this.Load += new System.EventHandler(this.frmAddExporter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackCompression)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lstFiles;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAddSingle;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAddMulti;
        private System.Windows.Forms.TrackBar trackCompression;
        private System.Windows.Forms.Label lblSWFCompression;
    }
}