﻿namespace SwiftRiver
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAddManifest = new System.Windows.Forms.Button();
            this.lstConfigs = new System.Windows.Forms.ListBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnSelectPaths = new System.Windows.Forms.Button();
            this.btnExportAll = new System.Windows.Forms.Button();
            this.btnExportMaster = new System.Windows.Forms.Button();
            this.btnExportIndividual = new System.Windows.Forms.Button();
            this.btnLoadManifest = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnLoadManifest);
            this.groupBox1.Controls.Add(this.btnRemove);
            this.groupBox1.Controls.Add(this.btnAddManifest);
            this.groupBox1.Controls.Add(this.lstConfigs);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(282, 240);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Swf Manifests";
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(8, 198);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(73, 35);
            this.btnRemove.TabIndex = 2;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAddManifest
            // 
            this.btnAddManifest.Location = new System.Drawing.Point(87, 198);
            this.btnAddManifest.Name = "btnAddManifest";
            this.btnAddManifest.Size = new System.Drawing.Size(84, 35);
            this.btnAddManifest.TabIndex = 1;
            this.btnAddManifest.Text = "Add Manifest";
            this.btnAddManifest.UseVisualStyleBackColor = true;
            this.btnAddManifest.Click += new System.EventHandler(this.btnAddManifest_Click);
            // 
            // lstConfigs
            // 
            this.lstConfigs.FormattingEnabled = true;
            this.lstConfigs.Location = new System.Drawing.Point(8, 19);
            this.lstConfigs.Name = "lstConfigs";
            this.lstConfigs.Size = new System.Drawing.Size(264, 173);
            this.lstConfigs.TabIndex = 0;
            this.lstConfigs.DoubleClick += new System.EventHandler(this.lstConfigs_DoubleClick);
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(8, 253);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(279, 38);
            this.btnExport.TabIndex = 1;
            this.btnExport.Text = "Export Selected";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnSelectPaths
            // 
            this.btnSelectPaths.Location = new System.Drawing.Point(7, 380);
            this.btnSelectPaths.Name = "btnSelectPaths";
            this.btnSelectPaths.Size = new System.Drawing.Size(279, 38);
            this.btnSelectPaths.TabIndex = 2;
            this.btnSelectPaths.Text = "Select Dependencies\' Paths";
            this.btnSelectPaths.UseVisualStyleBackColor = true;
            this.btnSelectPaths.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // btnExportAll
            // 
            this.btnExportAll.Location = new System.Drawing.Point(8, 294);
            this.btnExportAll.Name = "btnExportAll";
            this.btnExportAll.Size = new System.Drawing.Size(279, 38);
            this.btnExportAll.TabIndex = 3;
            this.btnExportAll.Text = "Export All";
            this.btnExportAll.UseVisualStyleBackColor = true;
            this.btnExportAll.Click += new System.EventHandler(this.btnExportAll_Click);
            // 
            // btnExportMaster
            // 
            this.btnExportMaster.Location = new System.Drawing.Point(8, 337);
            this.btnExportMaster.Name = "btnExportMaster";
            this.btnExportMaster.Size = new System.Drawing.Size(123, 38);
            this.btnExportMaster.TabIndex = 4;
            this.btnExportMaster.Text = "Export Master Manifest";
            this.btnExportMaster.UseVisualStyleBackColor = true;
            this.btnExportMaster.Click += new System.EventHandler(this.btnExportMaster_Click);
            // 
            // btnExportIndividual
            // 
            this.btnExportIndividual.Location = new System.Drawing.Point(137, 337);
            this.btnExportIndividual.Name = "btnExportIndividual";
            this.btnExportIndividual.Size = new System.Drawing.Size(149, 38);
            this.btnExportIndividual.TabIndex = 5;
            this.btnExportIndividual.Text = "Export Individual Manifests";
            this.btnExportIndividual.UseVisualStyleBackColor = true;
            this.btnExportIndividual.Click += new System.EventHandler(this.btnExportIndividual_Click);
            // 
            // btnLoadManifest
            // 
            this.btnLoadManifest.Location = new System.Drawing.Point(177, 198);
            this.btnLoadManifest.Name = "btnLoadManifest";
            this.btnLoadManifest.Size = new System.Drawing.Size(94, 35);
            this.btnLoadManifest.TabIndex = 3;
            this.btnLoadManifest.Text = "Load Master";
            this.btnLoadManifest.UseVisualStyleBackColor = true;
            this.btnLoadManifest.Click += new System.EventHandler(this.btnLoadManifest_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 426);
            this.Controls.Add(this.btnExportIndividual);
            this.Controls.Add(this.btnExportMaster);
            this.Controls.Add(this.btnExportAll);
            this.Controls.Add(this.btnSelectPaths);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "Swift River";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lstConfigs;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAddManifest;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnSelectPaths;
        private System.Windows.Forms.Button btnExportAll;
        private System.Windows.Forms.Button btnExportMaster;
        private System.Windows.Forms.Button btnExportIndividual;
        private System.Windows.Forms.Button btnLoadManifest;
    }
}

