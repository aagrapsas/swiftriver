﻿namespace SwiftRiver
{
    partial class frmExportFileSingle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkCompress = new System.Windows.Forms.CheckBox();
            this.chkSWFCompression = new System.Windows.Forms.CheckBox();
            this.lblSWFCompression = new System.Windows.Forms.Label();
            this.trackCompression = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.trackCompression)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(8, 24);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(268, 20);
            this.txtName.TabIndex = 0;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(8, 111);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(240, 20);
            this.txtPath.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "File";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(253, 111);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(26, 19);
            this.btnSelect.TabIndex = 3;
            this.btnSelect.Text = "...";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(156, 176);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(123, 28);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cboType
            // 
            this.cboType.FormattingEnabled = true;
            this.cboType.Items.AddRange(new object[] {
            "XML",
            "Image",
            "Animation",
            "Audio"});
            this.cboType.Location = new System.Drawing.Point(8, 68);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(266, 21);
            this.cboType.TabIndex = 1;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Type";
            // 
            // chkCompress
            // 
            this.chkCompress.AutoSize = true;
            this.chkCompress.Location = new System.Drawing.Point(170, 137);
            this.chkCompress.Name = "chkCompress";
            this.chkCompress.Size = new System.Drawing.Size(109, 17);
            this.chkCompress.TabIndex = 14;
            this.chkCompress.Text = "JPG Compression";
            this.chkCompress.UseVisualStyleBackColor = true;
            this.chkCompress.CheckedChanged += new System.EventHandler(this.chkCompress_CheckedChanged);
            // 
            // chkSWFCompression
            // 
            this.chkSWFCompression.AutoSize = true;
            this.chkSWFCompression.Location = new System.Drawing.Point(11, 137);
            this.chkSWFCompression.Name = "chkSWFCompression";
            this.chkSWFCompression.Size = new System.Drawing.Size(156, 17);
            this.chkSWFCompression.TabIndex = 15;
            this.chkSWFCompression.Text = "Override SWF Compression";
            this.chkSWFCompression.UseVisualStyleBackColor = true;
            this.chkSWFCompression.CheckedChanged += new System.EventHandler(this.chkSWFCompression_CheckedChanged);
            // 
            // lblSWFCompression
            // 
            this.lblSWFCompression.AutoSize = true;
            this.lblSWFCompression.Location = new System.Drawing.Point(9, 159);
            this.lblSWFCompression.Name = "lblSWFCompression";
            this.lblSWFCompression.Size = new System.Drawing.Size(126, 13);
            this.lblSWFCompression.TabIndex = 17;
            this.lblSWFCompression.Text = "SWF Compression: 100%";
            // 
            // trackCompression
            // 
            this.trackCompression.Location = new System.Drawing.Point(2, 176);
            this.trackCompression.Maximum = 100;
            this.trackCompression.Name = "trackCompression";
            this.trackCompression.Size = new System.Drawing.Size(148, 45);
            this.trackCompression.TabIndex = 16;
            this.trackCompression.ValueChanged += new System.EventHandler(this.trackCompression_ValueChanged);
            // 
            // frmExportFileSingle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 209);
            this.Controls.Add(this.lblSWFCompression);
            this.Controls.Add(this.trackCompression);
            this.Controls.Add(this.chkSWFCompression);
            this.Controls.Add(this.chkCompress);
            this.Controls.Add(this.cboType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmExportFileSingle";
            this.Text = "Embed File";
            this.Load += new System.EventHandler(this.frmExportFileSingle_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackCompression)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cboType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkCompress;
        private System.Windows.Forms.CheckBox chkSWFCompression;
        private System.Windows.Forms.Label lblSWFCompression;
        private System.Windows.Forms.TrackBar trackCompression;
    }
}