﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SwiftRiver
{
    class ManifestWriter
    {
        public static void WriteManifest(List<ExportRule> rules, String path)
        {
            if (rules.Count == 0)
            {
                return;
            }

            StreamWriter writer = new StreamWriter(path);
            String prepend = "";

            if (rules.Count > 1)
            {
                writer.Write("<manifests>");
                prepend = "\t";
            }

            foreach (ExportRule rule in rules)
            {
                writer.WriteLine(prepend + "<manifest name=\"" + rule.Name + "\">");

                foreach (ExportFile file in rule.Files)
                {
                    int count = file.Paths.Count;

                    if (file.DoesSkipFileCompress)
                    {
                        count = (int)Math.Ceiling((double)count / 2);
                    }

                    writer.WriteLine(prepend + "\t<file name=\"" + file.Name + "\" type=\"" + file.Type + "\" count=\"" + count + "\"/>");
                }

                writer.WriteLine(prepend + "</manifest>");
            }

            if (rules.Count > 1)
            {
                writer.Write("</manifests>");
            }

            writer.Close();
        }
    }
}
