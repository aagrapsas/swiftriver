﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;

using System.IO;

namespace SwiftRiver
{
    public class SWFCompiler
    {
        public static void CompileSwf(String mxmlcPath, String actionScriptPath, String swfPath, String additionalOptions = null)
        {
            string options = "-debug=false -optimize=true -strict=true -static-link-runtime-shared-libraries=true";
            string output = "-output=\"" + swfPath + "\"";
            string input = actionScriptPath;

            if (additionalOptions != null)
            {
                options += " " + additionalOptions;
            }

            string args = options + " " + output + " " + input;

            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = mxmlcPath;
            info.Arguments = args;
            info.UseShellExecute = false;
            info.RedirectStandardOutput = true;

            Console.WriteLine("RUNNING MXMLC");
            Console.WriteLine("Command: " + args);

            using (Process process = Process.Start(info))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();

                    Console.Write(result);
                }
            }
        }
    }
}
