﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace SwiftRiver
{
    public class AS3FileWriter
    {
        public static void WriteRule(ExportRule rule, String quantLocation, int quantSpeed)
        {
            StringBuilder builder = new StringBuilder();

            bool containsAudio = rule.Files.Count > 0 && rule.Files[0].Type == "Audio";

            builder.AppendLine("package");
            builder.AppendLine("{");

            builder.AppendLine("\t// Automatically built AS3 file");
            builder.AppendLine("\t// Compiled using Swift River");
            builder.AppendLine("\t// Last updated: " + DateTime.Now.ToString());
            builder.AppendLine("\t// -------------------------------------------");
            builder.AppendLine("");

            builder.AppendLine("\timport flash.display.Sprite;");

            if (containsAudio)
            {
                builder.AppendLine("\timport mx.core.SoundAsset;");
            }

            builder.AppendLine("");

            builder.AppendLine("\tpublic class " + rule.Name + " extends Sprite");
            builder.AppendLine("\t{");

            /*
                XML
                Image
                Animation
                Audio
             * */

            String relativePathAppender = Config.RelativePath.Replace("\\", "/") + "/";

            foreach (ExportFile file in rule.Files)
            {
                builder.AppendLine("\t\t// " + file.Name);
                builder.AppendLine("\t\t// " + file.Type);

                int count = 0;

                bool didSkip = true;

                foreach (String path in file.Paths)
                {
                    String fileCount = (file.Paths.Count > 1 ? count.ToString() : "");

                    String formattedPath = relativePathAppender + path.Replace("\\", "/");

                    Console.WriteLine("Formatted path: " + formattedPath);

                    //, compression=true, quality=90 here
                    String additional = "";

                    if (!file.DoesSkipFileCompress || didSkip)
                    {
                        if (file.DoesJPGCompress)
                        {
                            int quality = file.DoesSWFCompression ? file.SWFCompression : rule.SWFCompression;

                            additional = ", compression=true, quality=" + quality;
                        }
                        else if (formattedPath.Substring(formattedPath.LastIndexOf(".") + 1) == "png")
                        {
                            // png's have a special path for compression
                            String compressedLocation = Application.StartupPath + "\\converted\\" + rule.Name;// +"\\" + file.Name + ".png";
                            compressedLocation = Config.ConvertToRelative(compressedLocation);

                            if (Directory.Exists(compressedLocation) == false)
                            {
                                Directory.CreateDirectory(compressedLocation);
                            }

                            compressedLocation += "\\" + file.Name + fileCount + ".png";

                            FileInfo fileInfo = new FileInfo(formattedPath);

                            Boolean shouldRebuild = true;

                            if (File.Exists(compressedLocation))
                            {
                                FileInfo compressedInfo = new FileInfo(compressedLocation);

                                if (fileInfo.LastWriteTime.Equals(compressedInfo.LastWriteTime))
                                {
                                    shouldRebuild = false;
                                }
                            }

                            if (shouldRebuild)
                            {
                                // Remove existing compressed file
                                if (File.Exists(compressedLocation))
                                {
                                    File.Delete(compressedLocation);
                                }

                                // Generate a copy
                                File.Copy(formattedPath, compressedLocation);

                                // Go through and compress with pngquant
                                ProcessStartInfo processInfo = new ProcessStartInfo();
                                processInfo.FileName = quantLocation;
                                processInfo.Arguments = "-speed " + quantSpeed + " -force -ext .png " + compressedLocation;
                                processInfo.RedirectStandardOutput = true;
                                processInfo.UseShellExecute = false;

                                Console.WriteLine("Running quant on " + compressedLocation + " with args " + processInfo.Arguments);

                                // Setup reading into the console
                                using (Process newProcess = Process.Start(processInfo))
                                {
                                    using (StreamReader reader = newProcess.StandardOutput)
                                    {
                                        string result = reader.ReadToEnd();

                                        Console.Write(result);
                                    }

                                    // Wait for this process to complete before continuing (otherwise we might try to compile a swf before it's ready)
                                    newProcess.WaitForExit();
                                }
                            }
                            else
                            {
                                Console.WriteLine("File " + rule.Name + "/" + file.Name + " is already up-to-date");
                            }

                            // Mark original as having been written to (to prevent additional writing to)
                            FileInfo newFileInfo = new FileInfo(compressedLocation);
                            File.SetLastAccessTime(formattedPath, newFileInfo.LastWriteTime);

                            // Set path so swf compiles to the correct location
                            formattedPath = compressedLocation.Replace("\\", "/");
                        }

                        if (file.Type == "XML")
                        {
                            additional += ", mimeType = \"application/octet-stream\"";
                        }

                        builder.AppendLine("\t\t[Embed ( source=\"" + formattedPath + "\"" + additional + " )]");
                        builder.AppendLine("\t\tpublic var " + file.Name + fileCount + ":Class;");

                        count++;

                        didSkip = false;
                    }
                    else
                    {
                        didSkip = true;
                    }
                }

                builder.AppendLine("");
            }

            builder.AppendLine("\t}");
            builder.AppendLine("}");

            builder.AppendLine("// EOF");

            rule.IsDirty = false;

            StreamWriter writer = new StreamWriter(rule.Name + ".as");
            writer.Write(builder.ToString());
            writer.Close();
        }
    }
}
