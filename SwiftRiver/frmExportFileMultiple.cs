﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SwiftRiver
{
    public partial class frmExportFileMultiple : Form
    {
        public ExportFile File { get; set; }

        public frmExportFileMultiple()
        {
            InitializeComponent();
        }

        private void frmExportFileMultiple_Load(object sender, EventArgs e)
        {
            if (File == null)
            {
                File = new ExportFile();
            }
            else
            {
                foreach (String path in File.Paths)
                {
                    lstFiles.Items.Add(path);
                }

                txtName.Text = File.Name;
                cboType.Text = File.Type;
                chkSkip.CheckState = File.DoesSkipFileCompress ? CheckState.Checked : CheckState.Unchecked;

                if (File.DoesJPGCompress)
                {
                    chkCompress.Checked = true;
                }
            }

            trackCompression.Value = File.SWFCompression;
            chkSWFCompression.Checked = File.DoesSWFCompression;
            trackCompression.Enabled = File.DoesSWFCompression;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Title = "Select a file to load into the swf";
            dialog.Multiselect = true;

            DialogResult result = dialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            foreach (String fileName in dialog.FileNames)
            {
                lstFiles.Items.Add(Config.ConvertToRelative(fileName));
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            String path = lstFiles.SelectedItem as String;

            lstFiles.Items.Remove(path);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            File.Paths = new List<string>();

            foreach (String item in lstFiles.Items)
            {
                File.Paths.Add(item);
            }

            File.Type = cboType.Text;

            File.Name = txtName.Text;

            File.DoesSkipFileCompress = chkSkip.CheckState == CheckState.Checked;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;

            this.Close();
        }

        private void chkCompress_CheckedChanged(object sender, EventArgs e)
        {
            File.DoesJPGCompress = chkCompress.Checked;
        }

        private void chkSWFCompression_CheckedChanged(object sender, EventArgs e)
        {
            Boolean isEnabled = chkSWFCompression.Checked;

            File.DoesSWFCompression = isEnabled;
            trackCompression.Enabled = isEnabled;
        }

        private void trackCompression_ValueChanged(object sender, EventArgs e)
        {
            File.SWFCompression = trackCompression.Value;
            lblSWFCompression.Text = "SWF Compression: " + File.SWFCompression + "%";
        }
    }
}
