﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SwiftRiver
{
    public partial class frmAddExporter : Form
    {
        public ExportRule Rule { get; set; }

        public frmAddExporter()
        {
            InitializeComponent();
        }

        private void frmAddExporter_Load(object sender, EventArgs e)
        {
            if (Rule != null)
            {
                txtName.Text = Rule.Name;
                txtPath.Text = Rule.ExportPath;

                trackCompression.Value = Rule.SWFCompression;

                foreach (ExportFile file in Rule.Files)
                {
                    if (file.DoesExist())
                    {
                        file.UpdateFileSize();
                        lstFiles.Items.Add(file);
                    }
                    else
                    {
                        System.Console.WriteLine("DANGER: File " + file.Name + " cannot be found!!!!!!!!!!!!!!!!!!");

                        lstFiles.Items.Add(file);
                    }
                }
            }
            else
            {
                Rule = new ExportRule();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmExportFileSingle exporter = new frmExportFileSingle();

            DialogResult result = exporter.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            exporter.File.UpdateFileSize();
            lstFiles.Items.Add(exporter.File);

            this.Text = "Exporter*";
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            ExportFile selected = lstFiles.SelectedItem as ExportFile;

            lstFiles.Items.Remove(selected);

            this.Text = "Exporter*";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Rule.Name = txtName.Text;
            Rule.ExportPath = txtPath.Text;

            Rule.Files = new List<ExportFile>();

            foreach (ExportFile file in lstFiles.Items)
            {
                Rule.Files.Add(file);
            }

            Rule.IsDirty = true;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();

            dialog.Title = "Select the destination to save your swf";
            dialog.DefaultExt = "*.swf";
            dialog.Filter = "Swf (*.swf)|*.swf";

            DialogResult result = dialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            Rule.ExportPath = Config.ConvertToRelative(dialog.FileName);

            txtPath.Text = Config.ConvertToRelative(dialog.FileName);

            this.Text = "Exporter*";
        }

        private void btnAddMulti_Click(object sender, EventArgs e)
        {
            frmExportFileMultiple exporter = new frmExportFileMultiple();

            DialogResult result = exporter.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            exporter.File.UpdateFileSize();
            lstFiles.Items.Add(exporter.File);

            this.Text = "Exporter*";
        }

        private void lstFiles_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lstFiles_DoubleClick(object sender, EventArgs e)
        {
            ExportFile file = lstFiles.SelectedItem as ExportFile;

            if (file == null)
            {
                return;
            }

            if (file.Paths.Count > 1)
            {
                frmExportFileMultiple exporter = new frmExportFileMultiple();

                exporter.File = file;

                DialogResult result = exporter.ShowDialog();

                if (result != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                int index = lstFiles.SelectedIndex;

                lstFiles.Items.RemoveAt(index);

                lstFiles.Items.Insert(index, exporter.File);

                this.Text = "Exporter*";
            }
            else
            {
                frmExportFileSingle exporter = new frmExportFileSingle();

                exporter.File = file;

                DialogResult result = exporter.ShowDialog();

                if (result != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                int index = lstFiles.SelectedIndex;

                lstFiles.Items.RemoveAt(index);

                lstFiles.Items.Insert(index, exporter.File);

                this.Text = "Exporter*";
            }
        }

        private void trackCompression_ValueChanged(object sender, EventArgs e)
        {
            lblSWFCompression.Text = "SWF Compression: " + trackCompression.Value + "%";
            Rule.SWFCompression = trackCompression.Value;
        }
    }
}
