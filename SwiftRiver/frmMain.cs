﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

using System.IO;

namespace SwiftRiver
{
    public partial class frmMain : Form
    {
        private Config AppConfig;
        private ExportConfig Manifest;

        [DllImport("Kernel32.dll")]
        static extern Boolean AllocConsole();

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnAddManifest_Click(object sender, EventArgs e)
        {
            frmAddExporter exporter = new frmAddExporter();

            DialogResult result = exporter.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            Manifest.ExportRules.Add(exporter.Rule);

            lstConfigs.DataSource = null;
            lstConfigs.DataSource = Manifest.ExportRules;

            Manifest.Save();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            if (!AllocConsole())
            {
                MessageBox.Show("Failed to allocate console!");
            }

            AppConfig = Config.Load();
            Manifest = ExportConfig.Load();

            if (AppConfig.DoesExist == false)
            {
                SelectPaths();
            }

            lstConfigs.DataSource = Manifest.ExportRules;
        }

        private void SelectPaths()
        {
            SelectFlexPath();
            SelectPNGQuantPath();

            AppConfig.Save();

            return;
        }

        private void SelectFlexPath()
        {
            OpenFileDialog flex = new OpenFileDialog();

            flex.DefaultExt = "*.exe";
            flex.Title = "Select the mxmlc.exe location";
            flex.Filter = "Executable (*.exe)|*.exe";

            DialogResult result = flex.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            AppConfig.FlexHome = flex.FileName;
        }

        private void SelectPNGQuantPath()
        {
            OpenFileDialog quant = new OpenFileDialog();

            quant.DefaultExt = "*.exe";
            quant.Title = "Select pngquant.exe location";
            quant.Filter = "Executable (*.exe)|*.exe";

            DialogResult result = quant.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            AppConfig.QuantPath = quant.FileName;
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            SelectPaths();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            ExportRule rule = lstConfigs.SelectedItem as ExportRule;

            Manifest.ExportRules.Remove(rule);

            Manifest.Save();

            lstConfigs.DataSource = null;
            lstConfigs.DataSource = Manifest.ExportRules;
        }

        private void lstConfigs_DoubleClick(object sender, EventArgs e)
        {
            if (lstConfigs.SelectedItem == null)
            {
                return;
            }

            ExportRule rule = lstConfigs.SelectedItem as ExportRule;

            frmAddExporter exporter = new frmAddExporter();

            exporter.Rule = rule;

            DialogResult result = exporter.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            lstConfigs.DataSource = null;
            lstConfigs.DataSource = Manifest.ExportRules;

            Manifest.Save();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (lstConfigs.SelectedItem == null)
            {
                return;
            }

            FolderBrowserDialog dialog = new FolderBrowserDialog();

            dialog.SelectedPath = Config.RelativePath;

            dialog.ShowNewFolderButton = true;

            DialogResult result = dialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            ExportRule rule = lstConfigs.SelectedItem as ExportRule;

            AS3FileWriter.WriteRule(rule, AppConfig.QuantPath, AppConfig.QuantSpeed);
            SWFCompiler.CompileSwf(AppConfig.FlexHome, Application.StartupPath + "\\" + rule.Name + ".as", dialog.SelectedPath + "\\" + rule.Name + ".swf");
        }

        private void btnExportAll_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            dialog.SelectedPath = Config.RelativePath;

            dialog.ShowNewFolderButton = true;

            DialogResult result = dialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            foreach (ExportRule rule in Manifest.ExportRules)
            {
                AS3FileWriter.WriteRule(rule, AppConfig.QuantPath, AppConfig.QuantSpeed);
                SWFCompiler.CompileSwf(AppConfig.FlexHome, Application.StartupPath + "\\" + rule.Name + ".as", dialog.SelectedPath + "\\" + rule.ExportPath);
            }
        }

        private void SelectManifest()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            dialog.ShowNewFolderButton = true;

            DialogResult result = dialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            AppConfig.MasterManifestPath = dialog.SelectedPath;

            AppConfig.Save();
        }

        private void btnExportMaster_Click(object sender, EventArgs e)
        {
            if (AppConfig.MasterManifestPath == null)
            {
                SelectManifest();
            }

            if (AppConfig.MasterManifestPath == null)
            {
                return;
            }

            ManifestWriter.WriteManifest(Manifest.ExportRules, AppConfig.MasterManifestPath + "\\manifest.xml");
        }

        private void btnExportIndividual_Click(object sender, EventArgs e)
        {
            // Select export path
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            dialog.ShowNewFolderButton = true;

            dialog.SelectedPath = Config.RelativePath;

            DialogResult result = dialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            foreach (ExportRule rule in Manifest.ExportRules)
            {
                List<ExportRule> temp = new List<ExportRule>();

                temp.Add(rule);

                ManifestWriter.WriteManifest(temp, dialog.SelectedPath + "\\" + rule.Name + ".xml");
            }
        }

        private void btnLoadManifest_Click(object sender, EventArgs e)
        {
            OpenFileDialog manifest = new OpenFileDialog();

            manifest.DefaultExt = "*.xml";
            manifest.Title = "Select master manifest";
            manifest.Filter = "XML Manifest (*.xml)|*.xml";

            DialogResult result = manifest.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            lstConfigs.DataSource = null;
            Manifest = ExportConfig.Load(manifest.FileName);
            lstConfigs.DataSource = Manifest.ExportRules;

            String relativePath = manifest.FileName;
            relativePath = relativePath.Substring(0, relativePath.LastIndexOf(System.IO.Path.DirectorySeparatorChar));

            Config.RelativePath = relativePath;
        }
    }
}
