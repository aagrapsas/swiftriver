﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Serialization;
using System.IO;

namespace SwiftRiver
{
    public class ExportFile
    {
        public String Type { get; set; }
        public List<String> Paths { get; set; }
        public String Name { get; set; }
        public Boolean DoesSkipFileCompress { get; set; }
        public Boolean DoesJPGCompress { get; set; }
        public int SWFCompression { get; set; }
        public Boolean DoesSWFCompression { get; set; }

        [XmlIgnore]
        public Double FileSize { get; set; }

        public ExportFile()
        {
            Paths = new List<string>();
            SWFCompression = 90;
            DoesSWFCompression = false;
        }

        public Boolean DoesExist()
        {
            Boolean doesExist = true;

            foreach (String path in Paths)
            {
                if (File.Exists(Config.RelativePath + "\\" + path) == false)
                {
                    doesExist = false;
                    break;
                }
            }

            return doesExist;
        }

        public void UpdateFileSize()
        {
            double size = 0;

            try
            {
                foreach (String path in Paths)
                {
                    FileInfo info = new FileInfo(Config.RelativePath + System.IO.Path.DirectorySeparatorChar + path);
                    size += (info.Length / 1024);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e);
            }
            

            FileSize = size;
        }

        public override string ToString()
        {
            String size = FileSize.ToString();
            
            if (size.Contains("."))
            {
                size = size.Substring(0, Math.Min(size.IndexOf(".") + 2, size.Length));
            }

            return Name + " [" + size + " kb]";
        }
    }
}
