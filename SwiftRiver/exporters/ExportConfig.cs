﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Xml.Serialization;

namespace SwiftRiver
{
    public class ExportConfig
    {
        private static String Path = System.Environment.CurrentDirectory + "\\manifest.xml";

        public List<ExportRule> ExportRules { get; set; }

        [XmlIgnore]
        public Boolean DoesExist { get; private set; }

        public ExportConfig() 
        {
            ExportRules = new List<ExportRule>();
        }

        public static ExportConfig Load(String manifestPath)
        {
            Path = manifestPath;

            return Load();
        }

        public static ExportConfig Load()
        {
            ExportConfig config = new ExportConfig();

            if (File.Exists(Path))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ExportConfig));

                    TextReader reader = new StreamReader(Path);

                    config = (ExportConfig)serializer.Deserialize(reader);

                    reader.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                config.DoesExist = true;
            }
            else
            {
                config.DoesExist = false;
            }

            return config;
        }

        public void Save()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ExportConfig));

                TextWriter writer = new StreamWriter(Path);

                serializer.Serialize(writer, this);

                writer.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
