﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Serialization;

namespace SwiftRiver
{
    public class ExportRule
    {
        public List<ExportFile> Files { get; set; }
        public String ExportPath { get; set; }
        public String Name { get; set; }
        public int SWFCompression { get; set; }

        [XmlIgnore]
        public Boolean IsDirty { get; set; }

        public ExportRule()
        {
            SWFCompression = 90;
            Files = new List<ExportFile>();
        }

        public ExportRule(String name, List<ExportFile> files, String exportPath)
        {
            this.Name = name;
            this.Files = files;
            this.ExportPath = exportPath;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
